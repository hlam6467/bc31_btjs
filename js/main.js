// https://62bd9567bac21839b6069b0e.mockapi.io/
import { monAnController } from "./monAnController.js";
import { monAnService } from "./monAnService.js";
import { spinnerService } from "./spinnerService.js";
let foodList = [];
let idFoodList = null;
//in table
let renderTable = (list) => {
    let contentHTML = "";
    for (let i = 0; i < list.length; i++) {
        let monAn = list[i]
        let contentTR = `<tr> 
                            <td> ${monAn.id} </td>
                            <td> ${monAn.name} </td>
                            <td> ${monAn.price} </td>
                            <td> ${monAn.description} </td>
                            <td> 
                            <button onclick= "layDanhSachMonAn(${monAn.id})" class="btn btn-success"> Sửa </button>
                            <button onclick= "xoaMonAn(${monAn.id})" class="btn btn-danger"> Xoá </button>
                            </td>
                            </tr>`;
        contentHTML = contentHTML + contentTR;
    }
    document.getElementById("tbody_food").innerHTML = contentHTML;
    // render danh sách món ăn ra ngoài màn hình
};

let renderDachSachService = () => {
    spinnerService.batLoading();
    monAnService
        .layDanhSachMonAn()
        .then((res) => {
            spinnerService.tatLoading();

            foodList = res.data;

            renderTable(foodList)

        })
        .catch((err) => {
            spinnerService.tatLoading();
        });
}


// thêm món ăn
let themMonAn = () => {
    // sử dụng monAnController
    let monAn = monAnController.layThongTinTuForm();
    spinnerService.batLoading();

    monAnService
        .themMonAnMoi(monAn)
        .then((res) => {
            renderDachSachService();
            spinnerService.tatLoading();
        })
        .catch((err) => {
            alert("Thêm món ăn thất bại")
            spinnerService.tatLoading();

        })
};

window.themMonAn = themMonAn;

//cập nhập or lấy chi tiết món ăn
let layThongTinMonAn = (idMonAn) => {
    // gán id món ăn được chọn vào biến idFoodList
    idFoodList = idMonAn;
    spinnerService.batLoading();
    monAnService
        .layThongTinMonAn(idMonAn)
        .then((res) => {
            spinnerService.tatLoading();
            // lấy dữ liệu show lên giao diện (banding dữ liệu (data banding))
            monAnController.showThongTinLenForm(res.data)
        })
        .catch((err) => {
            spinnerService.tatLoading();
        })
};

window.layDanhSachMonAn = layThongTinMonAn;

// xoá món ăn
let xoaMonAn = (idMonAn) => {
    spinnerService.batLoading();
    monAnService
        .xoaMonAn(idMonAn)
        .then((res) => {
            spinnerService.tatLoading();
            // sau khi xoá món ăn thành công
            renderDachSachService()
        })
        .catch((err) => {
            spinnerService.tatLoading();
        })
}

window.xoaMonAn = xoaMonAn;

// chạy lần đầu khi load lại trang
renderDachSachService()

// cập nhập món ăn

let capNhapMonAn = () => {
    let monAn = monAnController.layThongTinTuForm()
    spinnerService.batLoading()
    let newMonAn = { ...monAn, id: idFoodList }
    monAnService
        .capNhapMonAn(newMonAn)
        .then((res) => {
            spinnerService.tatLoading()
            monAnController.showThongTinLenForm({
                name: "",
                price: "",
                description: "",
            });
            renderDachSachService()
        })
        .catch((err) => {
            spinnerService.tatLoading()
        })
}

window.capNhapMonAn = capNhapMonAn;

